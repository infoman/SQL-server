--USUARIO RORTIZ
--Por cada venta que se realiza se va restando la cantidad de productos
alter trigger realizaVenta
on Venta
for insert
as
begin
	declare @idProducto int
	declare @cantidad int
	set @idProducto = (select id_producto from inserted)
	set @cantidad = (select cantidad from inserted)
	if (select disponibilidad from Producto where id_producto = @idProducto) > @cantidad
	begin
		update Producto
		set disponibilidad = disponibilidad-@cantidad
		where id_producto = @idProducto
	end
	else
	begin
		select 'La cantidad no abastece'
	end
end	