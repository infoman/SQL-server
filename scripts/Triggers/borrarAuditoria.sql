create trigger borrarAuditoria
on Producto
for delete
as
begin
	declare @idProducto int
	set @idProducto = (select id_producto from deleted)
	if (select max(id_auditoria) from auditoria) is null
	begin
		insert into auditoria values (1, 1, @idProducto, CURRENT_TIMESTAMP, 1, 'DELETE')
	end
	else
	begin
		insert into auditoria values ((select max(id_auditoria)+1 from auditoria), 1, @idProducto, CURRENT_TIMESTAMP, 1, 'DELETE')
	end
end


