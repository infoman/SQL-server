use PruebaVentas
go

select * from Producto
select * from Venta
insert into Venta values (8, 2, 1, CURRENT_TIMESTAMP)
select * from Producto
select * from Venta

alter table Venta
disable trigger realizaVenta

sp_helptrigger Venta