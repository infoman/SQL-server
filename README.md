# SQL SERVER

## Union de tablas

### Inner Join

Funciona con multiples tablas, solo muestra las filas que tienen elementos comunes entre tablas.

```sql
select p.nombres, p.apPaterno, sum(i.monto) as impuesto
from automovil a
inner join persona p on p.id = a.propietario
inner join impuesto i on i.automovil = a.id
group by p.nombres, p.apPaterno
having sum(i.monto)
```

### Left Join

Funciona solo con dos tablas, donde muestra todas las filas de la izquierda, aunque estas no tengan relaciones.

```sql
select p.nombres, p.apPaterno, a.marca, a.modelo, i.monto as Impuesto
from persona p
inner join automovil a on p.id = a.propietario
inner join impuesto i on a.id = i.automovil
```

### Right Join

Funciona solo con dos tablas, donde muestra todas las filas de la derecha, aunque estas no tengan relaciones.

```sql
select p.nombres, p.apPaterno, a.marca as Impuesto
from persona p
right join automovil a on p.id = a.propietario
```

### Vistas
#### Creación de vistas

Las vistas son tablas virtuales creadas a partir de una consulta, como se muestra en el siguiente ejemplo:

```sql
create view ImpuestoPorCarro as
/*Sentencia SQL*/

select p.nombres, p.apPaterno, p.aMaterno, a.marca, a.modelo, sum(i.monto) Impuesto
from automovil a
inner join persona p on p.id = a.propietario
inner join impuesto i on a.id = i.automovil
group by p.nombres, p.apPaterno, p.aMaterno, a.marca, a.modelo
```

#### Cifrado de vistas

Primeramente para saber como es la consulta que compone la vista, se recurre al comando **sp_helptext**.

```sql
sp_helptext ImpuestoPorCarro
```

Entonces para crear una vista con encriptación, se hace de la siguiente forma:

```sql
create view ImpuestoPorPersonaEncrypt with Encryption as
/*Sentencia SQL*/
select p.nombres, p.apPaterno, p.aMaterno, sum(i.monto) Impuesto
from automovil a
inner join persona p on p.id = a.propietario
inner join impuesto i on i.automovil = a.id
group by p.nombres, p.apPaterno, p.aMaterno
```

#### Eliminar Vistas

El comando para eliminar vistas es **drop view**.

```sql
drop view ImpuestoPorPersona
```

#### Desripción de la tabla

Para saber el tipo de campos que tiene una tabla se usa el comando **desc**

```sql
DESC nombre_tabla
```

En SQL-Server se útiliza el comando **exec sp_columns**.

```sql
exec sp_columns nombre_tabla
```

#### Crear un nueva tabla a partir de otra existente

La tabla **nueva_personaJ** sera creada a partir de la tabla existente **persona** y esta nueva tabla constara de todas las personas que su nombre empiece con la letra "j".

```sql
select nombres, apPaterno, aMaterno into nueva_personaJ
from persona
where nombres like 'j%'
```

#### Estructura CASE

La expresión CASE se útiliza para evaluar varias expresiones:

```sql
select nombres, apPaterno, aMaterno, ci, procedencia, Origen = 
	case procedencia
		when 'LP' then 'Paceño'
		when 'CBBA' then 'Cochala'
		when 'SC' then 'Camba'
		else 'No tiene origen'
	end
from persona
```

```sql
SELECT *, TipoDeudor=
	case 
		when Impuesto >= 750 then 'Deudor moroso'
		else 'No debe mucho'
	end
FROM ImpuestoPorCarro
```

#### Condicional IF

Sirve para preguntar si una condición es Verdadera o falsa.

```sql
if exists(select * from persona where procedencia = 'LP')
	select * from persona where procedencia = 'LP'
else
	select 'No hay personas de La Paz'
```

### VARIABLES

Las sintaxis de las varibles esta compuesta de la siguiente manera:

```sql
/**VARIABLES EN SQL SERVER**/

--Declaración de variables
declare @marca varchar(50)
declare @modelo int
declare @totalImpuestos int

--Asignación de variables
set @modelo = 2000
set @totalImpuestos = (select sum(monto) from impuesto)

--Variables dentro una consulta
select * from automovil where modelo >= @modelo

--Variables dentro de un IF
if(@totalImpuestos > 1000000)
	select 'La deuda es mayor a 1000 Sus'
else
	select 'La deuda es menor a 1000 Sus'
```

### VARIABLES TIPO TABLA

Declaración de variables.

```sql
declare @tabla table( --Declaración de una variable tipo tabla
	nombres varchar(50),
	ci int
)

insert @tabla select nombres, ci from persona --Inserción de datos en una variable tipo tabla

select * from @tabla
```

### PROCEDIMIENTOS ALMACENADOS

Un procedimiento almacenado es un conjuno de sentencias almacenados en el servidor, ne la base de datos **master** con el prefijo **sp**. Un procedimiento alamcenado no puede tener:

* Create Procedure.
* Default.
* Ruler.
* Trigger.
* View.

Sintaxis

```sql
--Creación del procedimiento almacenado
create procedure mayores2000 as
select marca, modelo
from automovil
where modelo >= 2000

--Ejecución del procedimiento almacenado
exec mayores2000
```


### ELIMINACIÓN DE PROCEDIMIENTOS ALMACENADOS

Para la eliminación de un procedimiento almacenado, se utiliza el comando **drop proc** y el nombre del procedimiento almacenado.

```sql
drop proc mayores2000
```

Antes de eliminar, primeramente debemos verficar si el procedimientos almacenado existe, por lo que se útiliza la siguiente consulta:

```sql
if object_id('mayores2000') is not null
	drop proc mayores2000 /*Si existe el procedimiento almacenado, este se elimina*/
else
	select 'No existe' /*Si el procedimiento no almacenado existe, nos aparece un mensaje de que no existe*/
```

### PROCEDIMIENTOS ALMACENADOS CON PARÁMETROS DE ENTRADA

La creación un procedimiento con parámetros de entrada, se tiene la siguiente estructura:

```sql
create procedure nombre_procedimiento
	--Variables
as
	--Sentencias
```

**Ejemplo**

```sql
create procedure personAuto
	@cedula int,
	@marca varchar(50),
	@modelo int
as
	select nombres, apPaterno, aMaterno
	from persona
	where ci = @cedula

	select marca, modelo, color
	from automovil
	where marca = @marca or modelo = @modelo
```

Y se lo ejecuta de la siguiente forma:

```sql
exec personAuto 4792527, 'Toyota', 2000  /*exec nombre_procedimiento parámetros*/
```

Para inicializar las variables.

```sql
create procedure personAuto
	@cedula int = 4792527,
	@marca varchar(50) = 'Toyota',
	@modelo int = 2000
as
	select nombres, apPaterno, aMaterno
	from persona
	where ci = @cedula

	select marca, modelo, color
	from automovil
	where marca = @marca or modelo = @modelo
```

### PROCEDIMIENTOS ALMACENADOS CON PARÁMETROS DE SALIDA

Los procedimientos almacenados con parámetros de salida tienen la siguiente sintaxis.

```sql
create procedure mayoMenorImpuesto
	@mayor int output,
	@menor int output
as
	set @mayor = (select max(nn.impuesto) as imp
	from (select sum(i.monto) as impuesto
	from automovil a
	inner join impuesto i on a.id = i.automovil
	group by marca)nn)

	set @menor = (select min(nn.impuesto) as    imp
	from (select sum(i.monto) as impuesto
	from automovil a
	inner join impuesto i on a.id = i.automovil
	group by marca)nn)
```

Para hacer referencia a un procedimiento almacenado con parámetros de salida, se sigue la siguiente sintaxis.

```sql
declare @valorMax int
declare @valorMin int

exec mayoMenorImpuesto @valorMax output, @valorMin output

select @valorMax, @valorMin
```

### PROCEDIMIENTOS ALMACENADOS CON RETURN

Se útiliza igual que los procedimientos anteriores, con excepción de que se coloca la sentencia **return** al valor que se quiere devolver, como se muestra en el ejemplo de abajo:

```sql
create procedure sizeCad 
	@cadena varchar(50)
as
	if len(@cadena)%2 = 0
		return len(@cadena)/2
	else
		return (len(@cadena)+1)/2
```

Y la forma de llamar a este procedimiento es de la siguiente forma:

```sql
declare @size int --Declaración de variable
exec @size = sizeCad 'Rafael' --Ejecución del procedimiento enviando el parámetro de entrada 'Rafael'
select @size --Variable mostrada
```

#### Información de los procedimientos almacenados

```sql
sp_help --Muestra todos los objetos dentro la base de datos

sp_helptext nombre_objeto --Muestra la estructura del objeto

sp_stored_procedures --Muestra todos los procedimientos almacenados

sp_depends nombre_objeto --Muestra todos los objetos que dependen de nombre_objeto
```

#### Cifrado de procedimiento almacenados

Para cifrar un procedimiento se útiliza el comando **with encryption**

```sql
alter procedure actualizaAutomovil
	@marca varchar(50),
	@color varchar(50)
with encryption
as
	if (select propietario from automovil where marca = @marca and color = @color) is not null
	begin
		update automovil set modelo = 2017 where marca = @marca and color = @color
	end
	else
	begin
		print 'Propietario nulo'
	end
```

#### Estructuras repetitivas

Sintaxis del ciclo **While**

```sql
declare @w int
set @w = 0 --inicialización de la variable
while @w <= 8 --Mientras la variable sea menor que 8
begin
 print @w --Muestra un mensaje
 select * from automovil where id = @w
 set @w = @w+1 --Incrementa la variable en uno
end
```

#### Funciones Escalares

Se útilizan para calculos basicos, matemáticos ya que devuelven un único valor (un escalar).

```sql
create function impuestoValorAgregado(@valor money)
Returns money -- El tipo de valor que devolvera la función
as
Begin
	Declare @resultado money
	set @resultado = @valor*0.13
	return @resultado --Devuelve el resultado
End 
```
Llamada a la función **impuestoValorAgregado()**.

```sql
select marca, modelo, color, monto, dbo.impuestoValorAgregado(monto) as IVA from automovil a
inner join impuesto i on a.id = i.automovil
```

#### Funciones con valores de tabla de varias instrucciones

Estas tipo de funciones a diferencia de la anterior devuelven tablas como resultado.

**Ejemplo**

```sql
create function listaAutos(@propietario varchar(50))
returns @autoList table( --El valor tipo tabla que devolvera las función
	marcas varchar(50),  
	color varchar(50)	 --Los campos de la tabla
)
as
begin
	insert @autoList select a.marca, a.color
	from automovil a
	inner join persona p on a.propietario = p.id
	where p.nombres = @propietario
	return
end
```

Y se las invoca de la siguiente forma:

```sql
select * from dbo.listaAutos('Rafael Freddy')
```

#### Funciones con valores de tabla en linea

Al igual que la anterior, pero sin la necesidad de declarar varibles tipo tabla.

```sql
create function listaAutos_2(@propietario varchar(50))
returns table
as
return(
	select a.marca, a.color
	from automovil a
	inner join persona p on a.propietario = p.id
	where p.nombres = @propietario
)

--Invocación de la función
select * from listaAutos_2('Matias')
```


### Triggers o Disparadores

Un trigger es un tipo de procedimiento almacenado que se ejecuta, cuando se intenta modificar una tabla, no recibe ni devuelve parámetros.

#### Funcion GETDATE()

Esta función se utiliza para obtener la fecha actual, como se ve en el ejemplo.

~~~sql
insert into empleados values ('xxx7', 'Juna7', 'abc7', 1200, 'juan7', GETDATE())
~~~

#### TRIGGERS

Estructura básica de un trigger. 

~~~sql
CREATE [OR REPLACE] TRIGGER <nombre_trigger> 
{BEFORE|AFTER} 
{DELETE|INSERT|UPDATE 
[OF <col1>, ..., <colN>]              
[OR 
{DELETE|INSERT|UPDATE [OF 
<col1>, .., <colN>]
]}
ON
table <nombre_tabla> 
[
FOR EACH ROW 
[WHEN (<condicion>)]]
~~~

```sql
alter trigger insertPersonaBackup --Asignamos el nombre al trigger
on persona --Indicamos la tabla donde se activara la acción
for insert --Despues de insertar en la tabla persona
as
begin --Comienza la sentencia
	insert into personaBackup (nombres, ap_paterno, ap_materno) values ('aa1', 'bb1', 'cc1')
end --Finaliza la sentencia
```

#### Tablas temporales INSERTED, DELETED

La tabla temporal ***inserted*** lleva los datos que acabamos de insertar del nuevo registro con la declaración ***INSERT***.

La tabla temporal ***deleted*** lleva los datos que acabamos de borrar de un registro con la declaración ***DELETE***.

Con la declaración ***UPDATE*** los datos del registro se guardan en las tablas temporales ***inserted*** y ***deleted***.

#### Ejemplos de triggers avanzados

~~~sql
--USUARIO RORTIZ
--Por cada venta que se realiza se va restando la cantidad de productos
alter trigger realizaVenta
on Venta
for insert
as
begin
	declare @idProducto int
	declare @cantidad int
	set @idProducto = (select id_producto from inserted)
	set @cantidad = (select cantidad from inserted)
	if (select disponibilidad from Producto where id_producto = @idProducto) > @cantidad
	begin
		update Producto
		set disponibilidad = disponibilidad-@cantidad
		where id_producto = @idProducto
	end
	else
	begin
		select 'La cantidad no abastece'
	end
end	
~~~

#### Activar y desactivar Triggers

Para ver los triggers asociados a una tabla se escribe el siguiente comando:

~~~sql
sp_helptrigger NombreTabla
~~~

Para desabilitar un trigger se sigue la siguiente sintaxis.

~~~sql
alter table NombreTabla
disable trigger NombreTrigger
~~~

Para habilitar el trigger se útiliza el siguiente comando
~~~sql
alter table NombreTabla
enable trigger NombreTrigger
~~~

### CURSORES

Los cursores son una herramienta que nos permite recorrer las filas del resultado de una consulta y realizar operaciones en cada una de estas.

Con las siguientes lineas de comando se carga los datos de la tabla ***nombreTabla*** al cursor ***nombreCursor***:

~~~sql
DECLARE nombreCursor CURSOR
FOR select * from nombreTabla
~~~

Luego se tiene que abrir el cursor con el comando:

~~~sql
OPEN nombreCursor
~~~

Y para cerrarlo el comando:

~~~sql
CLOSE nombreCursor
~~~

Para liberar el cursor se utilizá la siguiente sentencia:

~~~sql
DEALLOCATE extraeProductosScroll
~~~

Para extraer los datos del cursor se utilizá el comando:

~~~sql
FETCH NEXT FROM nombreCursor
~~~

Para utilizar con las sentencias ***LAST*** y ***FIRST***. Al cursor se le debe aumentar el comando ***SCROLL*** de la siguiente forma:

~~~sql
DECLARE extraeProductosScroll SCROLL CURSOR
FOR select * from Producto
~~~

Utilizando la sentencia anterior para declarar cursores, se podría hacer uso de los comando  ***LAST*** y ***FIRST***, esto para extraer la primera y ultima fila del cursor.

~~~sql
FETCH FIRST FROM extraeProductosScroll
FETCH LAST FROM extraeProductosScroll
~~~

#### Recorrido del cursor

Para hacer el recorrido del cursor se utilizá la sentencia ***while***; la sentencia ***@@FETCH_STATUS***, la cual pregunta si existen mas filas:

Para extraer todas las filas del cursor se hará escribirá la siguiente sentencia:

~~~sql
FETCH NEXT FROM extraeProductos --Extrae la primera fila
WHILE @@FETCH_STATUS = 0  
BEGIN
	FETCH NEXT FROM extraeProductos
END  
~~~

#### Utilizar FETCH para almacenar valores en variables

Las salidas de las instrucción FETCH se almacena en variables locales, como en el siguiente ejemplo:

~~~sql
--Declaramos el cursor:
declare extraeProductos cursor
for select * from Producto

--Abrimos el cursor:
open extraeProductos

declare @id_prod int
declare @nombre varchar(50)
declare @precio int
declare @dispo int
declare @det varchar(100)

--extraemos la primera fila
FETCH NEXT FROM extraeProductos INTO @id_prod, @nombre, @precio, @dispo, @det;  

WHILE @@FETCH_STATUS = 0  
BEGIN
	print @nombre+' '+@det   
	FETCH NEXT FROM extraeProductos INTO @id_prod, @nombre, @precio, @dispo, @det
END 

--Cerramos el cursor
close extraeProductos
~~~








































